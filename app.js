require('dotenv').config();

const fs = require('fs');
const AWS = require('aws-sdk');

// The name of the bucket that you have created
const BUCKET_NAME = 'open-uploads';

const s3 = new AWS.S3({
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY,
    httpOptions: { 
        timeout: 60 * 60 * 1000 
    }
});

const uploadFile = (fileName) => {

    const readStream = fs.createReadStream(fileName); 

    const params = {
        Bucket: BUCKET_NAME,
        Key: fileName,
        Body: readStream
    };

    const options = { 
        partSize: 50 * 1024 * 1024, // 50MB
        queueSize: 1 
    };  

    s3.upload(params, options)
    .on('httpUploadProgress', function(evt) {
        console.log('Completed ' +  (evt.loaded * 100 / evt.total).toFixed(2) + '% of upload');  
    })    
    .send(function(err, data) {
        if (err) {
            throw err;
        }
        console.log(`File uploaded successfully. ${data.Location}`);  
    });

};

uploadFile('test/pro.img');
